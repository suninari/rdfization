import csv
import rdflib
from rdflib import Graph
from rdflib.namespace import RDF
from rdflib import URIRef, Literal, XSD

# ontology define
rxn = rdflib.Namespace("http://glycosmos.org/biopax/1/562#")
bp = rdflib.Namespace("http://www.biopax.org/release/biopax-level3.owl#")
owl = rdflib.Namespace("http://www.w3.org/2002/07/owl#")
chebi_SN = rdflib.Namespace("http://purl.obolibrary.org/obo/CHEBI_25609#")
glyco_RDF = rdflib.Namespace("http://purl.jp/bio/12/glyco/glycan#")
gno = rdflib.Namespace("http://purl.obolibrary.org/obo/")
identifier = rdflib.Namespace("https://glytoucan.org/Structures/Glycans/")

# CSV file open
f = open('GC-saccharide.csv', 'rt')
dataReader = csv.reader(f, delimiter=',')

# making graph
g = Graph()

for row in dataReader:
    # Instances of SmallMolecule Class
    col_A = row[0]
    col_B = row[1]
    col_C = row[2]
    col_D = row[3]
    col_E = row[4]
    col_F = row[5]
    col_G = row[6]


    if row[0] != "SmallMolecule":


        gc_sm= URIRef(rxn + col_A)
        sm_enti_Ref = URIRef(rxn + col_B)
        sm_name = Literal(col_C, datatype=XSD.string)
        if col_D == "Saccharide":
            sm_type = URIRef(glyco_RDF + "Saccharide")
        else:
            sm_type = URIRef("http://purl.obolibrary.org/obo/CHEBI_25609")

        sm_xref = URIRef(rxn + col_E)
        sm_bp_type = URIRef(bp + col_F)
        sm_toucan_id = URIRef(identifier + col_G)

        g.add((gc_sm, bp.entityReference, sm_enti_Ref))
        g.add((gc_sm, bp.displayName, sm_name))
        g.add((gc_sm, RDF.type, sm_type))
        g.add((gc_sm, bp.xref, sm_xref))
        g.add((gc_sm, RDF.type, sm_bp_type))
        g.add((gc_sm, gno.GNO_00000022, sm_toucan_id))

# data output

g.serialize(destination="GC_Saccharide_2020_02.ttl", format='turtle')