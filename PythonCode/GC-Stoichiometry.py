import csv
import rdflib
from rdflib import Graph
from rdflib.namespace import RDF
from rdflib import URIRef, BNode, Literal, XSD

# ontology
rxn = rdflib.Namespace("http://glycosmos.org/biopax/1/562#")
bp = rdflib.Namespace("http://www.biopax.org/release/biopax-level3.owl#")
owl = rdflib.Namespace("http://www.w3.org/2002/07/owl#")
# CSV
f = open('GC-Stoichiometry_saccharide.csv', 'rt')
dataReader = csv.reader(f, delimiter=',')

g = Graph()

for row in dataReader:

    col_A = row[0]
    col_B = row[1]
    col_C = row[2]
    col_D = row[3]


    if row[0] != "Stoichiometry":

        gc_sto= URIRef(rxn + col_A)
        sto_type = URIRef(bp + col_B)
        sto_entity = URIRef(rxn + col_C)
        sto_coef = Literal(col_D, datatype=XSD.integer)

        g.add((gc_sto, RDF.type, sto_type))
        g.add((gc_sto, bp.physicalEntity, sto_entity))
        g.add((gc_sto, bp.stoichiometryCoefficient, sto_coef))

g.serialize(destination="GC_Stoichiometry_RDF_Saccharide.ttl", format='turtle')