
import csv
import rdflib
from rdflib import Graph
from rdflib.namespace import RDF
from rdflib import URIRef

# ontology
rxn = rdflib.Namespace("http://glycosmos.org/biopax/1/562#")
bp = rdflib.Namespace("http://www.biopax.org/release/biopax-level3.owl#")
owl = rdflib.Namespace("http://www.w3.org/2002/07/owl#")
# CSV
f = open('GC-SaccharideReference.csv','rt')
dataReader = csv.reader(f, delimiter=',')

g = Graph()

for row in dataReader:
    # Class row of Instances
    col_A = row[0]
    col_B = row[1]
    col_C = row[2]

    if row[0] != "SmallMoleculeReference":

        ref_rxn= URIRef(rxn + col_A)
        ref_uni = URIRef(rxn + col_B)
        ref_type = URIRef(bp + col_C)

        g.add((ref_rxn, bp.xref, ref_uni))
        g.add((ref_rxn, RDF.type, ref_type))

g.serialize(destination="GC_SaccharideReference_RDF.ttl", format='turtle')