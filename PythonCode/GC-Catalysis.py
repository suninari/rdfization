import csv
import rdflib
from rdflib import Graph
from rdflib.namespace import RDF
from rdflib import URIRef, Literal, XSD
# ontology define
rxn = rdflib.Namespace("http://glycosmos.org/biopax/1/562#")
bp = rdflib.Namespace("http://www.biopax.org/release/biopax-level3.owl#")
owl = rdflib.Namespace("http://www.w3.org/2002/07/owl#")

# CSV file open
f = open('GC-catalysis.csv', 'rt')
dataReader = csv.reader(f, delimiter=',')

g = Graph()

for row in dataReader:
    # Instances of SmallMolecule Class
    col_A = row[0]
    col_B = row[1]
    col_C = row[2]
    col_D = row[3]
    col_E = row[4]

    if row[0] != "Catalysis":
        #print("into the if")
        gc_cat= URIRef(rxn + col_A)
        cat_controller = URIRef (rxn + col_B)
        cat_controlled = URIRef (rxn +col_C)
        cat_cotroltype = Literal(col_D, datatype=XSD.string) #datatype= XSD.string))
        cat_type= URIRef(bp + col_E)

        g.add((gc_cat, bp.controller, cat_controller))
        g.add((gc_cat, bp.controlled, cat_controlled))
        g.add((gc_cat, bp.controlType, cat_cotroltype))
        g.add((gc_cat, RDF.type, cat_type))


g.serialize(destination="GC_Catalysis_RDF.ttl", format='turtle')