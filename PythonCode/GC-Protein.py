import csv
import rdflib
from rdflib import Graph
from rdflib.namespace import RDF
from rdflib import URIRef, BNode, Literal, XSD

# ontology
rxn = rdflib.Namespace("http://glycosmos.org/biopax/1/562#")
bp = rdflib.Namespace("http://www.biopax.org/release/biopax-level3.owl#")
owl = rdflib.Namespace("http://www.w3.org/2002/07/owl#")

# CSV
f = open('GC-Protein.csv', 'rt')
dataReader = csv.reader(f, delimiter=',')

g = Graph()

for row in dataReader:
    col_A = row[0]
    col_B = row[1]
    col_C = row[2]
    col_D = row[3]
    col_E = row[4]
    col_F = row[5]

    if row[0] != "Protein":
        gc_pro= URIRef(rxn + col_A)
        pro_displayname = Literal(col_B,datatype=XSD.string)
        pro_uniref = URIRef(rxn + col_C)
        type_pro = URIRef(bp + col_D)
        pro_name = Literal(col_E, datatype=XSD.string)
        pro_ref = URIRef (rxn +col_F)

        if col_B !="":
            g.add((gc_pro, bp.displayName, pro_displayname))
        if col_E != "":
            g.add((gc_pro, bp.name, pro_name))

        g.add((gc_pro, bp.xref, pro_uniref))
        g.add((gc_pro, RDF.type, type_pro))
        g.add((gc_pro, bp.entityReference, pro_ref))

g.serialize(destination="GC_Protein_RDF_New.ttl", format='turtle')